# Review Chatgpt

Cоздать и активировать виртуальное окружение:

```
python3 -m venv venv
```
```
source venv/bin/activate
```

```
python3 -m pip install --upgrade pip
```

Установить зависимости из файла requirements.txt:

```
pip install -r requirements.txt
```

Запустить проект:

```
python3 main.py 
```
### Техническое Задание
* С помощью ChatGPT создайте python-скрипт, который сможет делать то же самое — оценивать отзывы из таблицы по интонации, используя OpenAI API. 
* Скрипт должен читать файл из директории, проставлять оценку в колонку rate, сортировать отзывы по убыванию оценки, 
* записывать в новый файл с названием вида “%filename%_analyzed.csv” и складывать в ту же директорию.