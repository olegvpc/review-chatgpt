import csv
import re

import openai
OPEN_API_KEY='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'
# Аутентификация в OpenAI API
openai.api_key = OPEN_API_KEY

PROMPT = 'analyze review after symbol "=>" and return the range: number from 1 to 10, where 10 is the most enthusiastic, and 1 is the most negative.'


def get_rating(text):
    query = f'{PROMPT} => {text}'
    # print(query)
    try:
        response = openai.Completion.create(
            model="text-davinci-003",
            prompt=query,
            temperature=0.3,
            max_tokens=1000,
            top_p=1.0,
            frequency_penalty=0.5,
            presence_penalty=0.0,
            stop=["You:"]
        )
        if response:
            # print(response["choices"][0]["text"])
            return response["choices"][0]["text"]

    except Exception as e:
        net_error = f'ChatGPT do not response: {e}.'
        print(net_error)


def write_file(data):
# Write to new file with name "%filename%_analyzed.csv"
    with open('reviews_analyzed.csv', 'w') as analyzed:
        writer = csv.DictWriter(analyzed, fieldnames=['email', 'review', 'date', 'rate'])
        # writer = csv.writer(analyzed)
        writer.writeheader()
        for item in data:
            print(item)  # {'email': 'bryantjames@example.com', 'date': '2023-01-15', 'rate': 0}

            writer.writerow(item)


def read_file():
    with open('reviews.csv', 'r') as reviews:
        # reader = csv.DictReader(reviews, restkey=None, restval=None, dialect='excel')  # DictReader - словарь
        reader = csv.reader(reviews)  # DictReader - list
        data = list([''.join(row).split(';') for row in reader])
        email_list = [item[0] for item in data]
        review_list = [item[1] for item in data]
        date_list = [item[2] for item in data]
        # print(email_list, date_list)
        data_object = []
        if len(email_list) == len(date_list):
            for i in range(len(email_list)):
                response = get_rating(review_list[i])
                if response:
                    response_range = (re.sub("\n\n|.+\n\n", "", response))  # regex for data-tail of response
                    data_object.append({'email': email_list[i], 'review': review_list[i], 'date': date_list[i],
                                        'rate': int(response_range)})

        def custom_key(rate):
            return rate['rate']

        data_object.sort(key=custom_key, reverse=True) # sorted object from bigger to miner

        write_file(data_object)


if __name__ == '__main__':
    read_file()